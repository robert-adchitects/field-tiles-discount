<?php

function woocommerce_rename_coupon_message_on_checkout() {
	return '';
}
// add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );


add_filter('woocommerce_coupon_error', 'rename_coupon_label', 10, 3);
add_filter('woocommerce_coupon_message', 'rename_coupon_label', 10, 3);
//add_filter('woocommerce_cart_totals_coupon_label', 'rename_coupon_label',10, 1);
function rename_coupon_label($err, $err_code=null, $something=null){
	$err = str_ireplace("Coupon","Discount code ",$err);
	return $err;
}

//coupon name wrapped in span
add_filter( 'woocommerce_cart_totals_coupon_label', 'cart_totals_coupon_label', 100, 2 );
function cart_totals_coupon_label($label, $coupon) {

	if ($coupon) {
    if ($coupon->is_type('percent')) {
      $val = $coupon->amount . '%';
    } else {
      $val = wc_price($coupon->amount);
    }
		$code = $coupon->get_code(); 
		if ($code == 'trade-account') {
      $label = 'Volume Discount<br><small>' . $val . ' trade&nbsp;discount</small>';
      $coupon_label = __($label, 'ft');
    } else {
     $coupon_label = __('Discount Code', 'ft') . '<span class="discount-name">' . $val . ' ' . $code . ' discount</span>';
   }
   return $coupon_label;
 }

 return $label;
}

//add discount if account = trade
add_action('woocommerce_before_calculate_totals', 'discount_based_on_trade_account', 5);
function discount_based_on_trade_account( $cart ) {
  var_dump($cart);
	if ( is_admin() && ! defined( 'DOING_AJAX' ) )
		return;

	if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
		return;

    $samples_discount = discount_based_on_samples_count($cart) ? discount_based_on_samples_count($cart) : 0;
    $coupon_code      = 'trade-account'; // Coupon code
    $coupon_amount = 5; //$cart->coupon_discount_totals['trade-account'] ? $cart->coupon_discount_totals['trade-account'] : 0;

    $user_id = get_current_user_id(); 
    if (get_user_meta($user_id, 'type')[0] == 'trade') {
    	$trade = true;
    } else {
    	$trade = false;
    }
    // Initializing variables
    $applied_coupons  = $cart->get_applied_coupons();
    $subtotal = floatval( preg_replace( '#[^\d.]#', '', $cart->get_cart_total() ) );
    $coupon_code      = sanitize_text_field( $coupon_code );
    $max_free = get_field('max_free', 'option') ? get_field('max_free', 'option') : 10; 

    if ($samples_discount > 0 && $trade == true) {
      $label = 'Samples Discount <br><small>' . $max_free . ' free&nbsp;trade&nbsp;samples</small>';

      $cart->add_fee($label, -$samples_discount);
      wc_clear_notices();
    }

    // Applying coupon 
    if( (!in_array($coupon_code, $applied_coupons) || $fees > 0) && $trade == true && $subtotal > 0){
     $cart->add_discount( $coupon_code );
     wc_clear_notices();
   }
 }


 add_action('woocommerce_before_calculate_totals', 'discount_based_on_samples_count', 5);
 function discount_based_on_samples_count( $cart ) {
  if ( is_admin() && ! defined( 'DOING_AJAX' ) )
    return;

  if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
    return;

  $fees = 0;
  $samples_count = 0;
  $max_free = get_field('max_free', 'option') ? get_field('max_free', 'option') : 10; 
  $sample_price = get_field('sample_price', 'option') ? get_field('sample_price', 'option') : 5; 
  foreach( $cart->get_cart() as $cart_item ){
    $product_id = $cart_item['product_id'];
    $_product_quantity = $cart_item['quantity']; 
    $_product = wc_get_product( $product_id );
    $_product_price = $_product->get_price();
    $name = $_product->get_title();

    if ($_product->is_type( 'simple' ) && has_term( 'sample', 'product_cat', $product_id )) {//add to fee if buy more than x samples
      //if ($samples_count <= $max_free) {
        $samples_count+=$_product_quantity;   
      // } else {
      //   $samples_count+=0;
      // }
        
    }
  }
    if ($samples_count <= $max_free) {
      $fees = $samples_count * $sample_price;
   } else {
     $fees = $max_free * $sample_price;
   }
  return $fees;
}

//strikethrough price
//add_filter( 'woocommerce_cart_item_subtotal', 'show_coupon_item_subtotal_discount', 100, 3 );
function show_coupon_item_subtotal_discount( $subtotal, $cart_item, $cart_item_key ){
  $product_id = $cart_item['data']->id;
  $product_type = $cart_item['product_type'];
  $_product_quantity = $cart_item['quantity']; 
  $_product = wc_get_product( $product_id );
  $_product_price = $_product->get_price();
  $max_free = get_field('max_free', 'option') ? get_field('max_free', 'option') : 10; 

  //show strikethrough forregular tiles
  if( $cart_item['line_subtotal'] !== $cart_item['line_total'] ) {
    $subtotal = sprintf( ' %s<del>%s</del>',  wc_price($cart_item['line_total']), wc_price($cart_item['line_subtotal']) );
  }

 //show strikethrough for samples
  if ($_product->is_type('simple') && has_term( 'sample', 'product_cat', $product_id )) {
    if ($_product_quantity > $max_free) {
      if ($max_free && has_term( 'sample', 'product_cat', $product_id )) {
        $subtotal = sprintf( ' %s<del>%s</del>', wc_price((($_product_quantity - $max_free)*$_product_price)), wc_price($cart_item['line_subtotal']) );
      }
    } else {
      $subtotal = sprintf( ' %s<del>%s</del>', wc_price(0), wc_price($_product_price*$_product_quantity) );
    }    
  }

  return $subtotal;
}


//update real coupon form with fake one values
add_action( 'wp_footer', 'woocommerce_tip_script' );
function woocommerce_tip_script() {
    // Only on checkout page
 if( ! is_checkout() ) return;
 ?>
 <script type="text/javascript">
  jQuery(function($){
   $('form.woocommerce-checkout').on( 'change', 'input[name="fake_coupon_code"]', function(){
    var coupon_name = $(this).val();
    $('input[name="coupon_code"]').val(coupon_name);
    $('.woocommerce-form-coupon button').click();
    $('body').trigger('update_checkout');
  });
 })
</script>
<?php
}

function str_replace_first($search, $replace, $subject) {
  $pos = strpos($subject, $search);
  if ($pos !== false) {
    return substr_replace($subject, $replace, $pos, strlen($search));
  }
  return $subject;
}

add_filter( 'woocommerce_coupon_discount_amount_html', 'filter_function_name_9134', 10, 2 );
function filter_function_name_9134( $discount_amount_html, $coupon ){
 $discount_amount_html = str_replace_first("-", "", $discount_amount_html);

 return $discount_amount_html;
}

